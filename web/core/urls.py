from django.contrib import admin
from django.urls import path, re_path
from django.conf import settings
from django.conf.urls.static import static
from page.views import Page, Order
from api.views import Api

urlpatterns = [
                  path('admin/', admin.site.urls),
                  path('api/', Api.as_view(), name='api'),
                  path(
                      'zakazat-spravku-o-stoimosti/', Order.as_view(),
                      {'slug': 'zakazat-spravku-o-stoimosti'},
                      name='zakazat-spravku-o-stoimosti'
                  ),
                  path(
                      'zakazat-ekspertnoe-issledovanie/', Order.as_view(),
                      {'slug': 'zakazat-ekspertnoe-issledovanie'},
                      name='zakazat-ekspertnoe-issledovanie'
                  ),
                  path(
                      'zakazat-otchet-ob-otsenke/', Order.as_view(),
                      {'slug': 'zakazat-otchet-ob-otsenke'},
                      name='zakazat-otchet-ob-otsenke'
                  ),
                  path(
                      'zakazat-stoimostnoe-zakliuchenie/', Order.as_view(),
                      {'slug': 'zakazat-stoimostnoe-zakliuchenie'},
                      name='zakazat-stoimostnoe-zakliuchenie'
                  ),
                  path(
                      'zakazat-ne-stoimostnoe-issledovanie/', Order.as_view(),
                      {'slug': 'zakazat-ne-stoimostnoe-issledovanie'},
                      name='zakazat-ne-stoimostnoe-issledovanie'
                  ),
                  re_path('(?P<slug>[0-9a-z.\-\[\]]+)/$', Page.as_view(), name='page'),
                  path('', Page.as_view(), {'slug': '/'}, name='home'),

              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
