import json
from django.views import View
from django.http import HttpResponse
from django.template import loader
from pay.models import PricePolicy
from page.models import Page as PageModel
from file.models import File


class Page(View):
    @staticmethod
    def get(request, **kwargs):
        page = PageModel.page_public.filter(slug=kwargs.get('slug')).get()
        template = loader.get_template(page.template if page.template else 'index.html')
        return HttpResponse(template.render({'page': page, 'content': page.content_set.get()}, request))


class Order(View):
    @staticmethod
    def get(request, **kwargs):
        page = PageModel.page_public.filter(slug=kwargs.get('slug')).get()
        price_policy = PricePolicy.objects.filter(nomenclature=page.nomenclature)
        template = loader.get_template('order.html')
        return HttpResponse(template.render(
            {
                'page': page,
                'content': page.content_set.order_by('order').all(),
                'files': File.get_file_by_token(token=request.COOKIES.get('Token')),
                'nomenclature': json.dumps({'key': page.nomenclature.pk}),
                'options': json.dumps(
                    [{
                        'name': x.name, 'key': x.pk, 'status': x.status_default,
                        'ratio': float(x.ratio)} for x in page.nomenclature.options.all()
                    ]),
                'price_policy': json.dumps(
                    [{'alias': x.pay.alias, 'name': x.pay.name, 'price': int(x.price)} for x in price_policy]
                )
            },
            request)
        )
