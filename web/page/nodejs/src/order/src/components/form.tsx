import * as React from "react";

declare var window: {
    [key:string]: any;
}
export default class Form extends React.Component<any, any> {

    render() {
        return <div>
            <div className="w3-section">
                <label>Ваше имя:
            <input className={`w3-input ${this.props.getErrorClass('firstName')}`} type="text" value={this.props.firstName} onChange={(e) => this.props.updateState({ firstName: e.target.value })} />
                    {this.props.error.firstName ? <span className="w3-text-red">{this.props.error.firstName}</span> : null}
                </label>
            </div>

            <div className="w3-section">
                <label>Номер телефона:
            <input className={`w3-input ${this.props.getErrorClass('phoneNumber')}`} type="text" value={this.props.phoneNumber} onChange={(e) => this.props.updateState({ phoneNumber: e.target.value })} />
                    {this.props.error.phoneNumber ? <span className="w3-text-red">{this.props.error.phoneNumber}</span> : null}
                </label>
            </div>

            <div className="w3-section">
                <label>Email:
            <input className={`w3-input ${this.props.getErrorClass('email')}`} type="text" value={this.props.email} onChange={(e) => this.props.updateState({ email: e.target.value })} />
                    {this.props.error.email ? <span className="w3-text-red">{this.props.error.email}</span> : null}
                </label>
            </div>

            <div className="w3-section">
                <label style={{ display: 'block', margin: '0 0 10px 0' }}>Дополнительная информация
            <textarea className="w3-input w3-border" style={{ resize: 'none' }} value={this.props.description} onChange={(e) => this.props.updateState({ description: e.target.value })} />
                </label>
            </div>

            <div className="w3-section">
                <label className="custom-file-upload w3-half">
                    <input type="file" name="file" multiple={true} ref="fileInput" onChange={this.props.fileUpload} />
                    <p style={{ margin: '7px' }}><i className="fa fa-cloud-upload" aria-hidden="true"></i> - нажмите для загрузки документов</p>
                </label>
            </div>
            <div className="w3-row">
                {this.props.files.length
                    ? <p><b>Загруженные файлы:</b>
                        <ul className="w3-ul w3-small">
                            {this.props.files.map((file: any, key: number) => (<li key={key} className="w3-display-container" onClick={() => { this.props.files.splice(key, 1); this.props.updateState({ files: this.props.files }) }} >{file.origin}
                                <span className="w3-button w3-display-right">&times;</span>
                            </li>))}
                        </ul></p>
                    : null}
            </div>

            <div className="w3-row">
                <p>Варианты оплаты получения справки о стоимости</p>
                <div className="w3-half w3-container">
                    {this.props.error.pay ? <span className="w3-text-red">{this.props.error.pay}</span> : null}
                    <div className="w3-row">
                        {this.props.pricePolicy.length
                            ? this.props.pricePolicy.map((price: any, key:number) => (
                                <div key={key}
                                    className={`w3-btn w3-white w3-ripple w3-half ${this.props.pay === price.alias ? 'w3-border w3-border-green' : ''}`}
                                    style={{ margin: '10px', padding: '0' }}
                                    onClick={() => this.props.updateState({ pay: price.alias, price: price.price })}>
                                    <img src={window[`payLogo_${price.alias}`]} title={price.name} alt={price.name} />
                                </div>
                            ))
                            : null}
                    </div>

                </div>

                <div className="w3-half w3-container w3-center" style={{ margin: '-30px 0 0 0' }}>
                    {this.props.nomenclatureOptions.length
                        ? <p> <ul className="w3-ul">{this.props.nomenclatureOptions.map((option: any, key:number) => (
                            <li key={key}>
                                <label className="switch" style={{ float: 'left' }}>
                                    <input type="checkbox" checked={this.props.options.includes(option.key)}
                                        onClick={() => this.props.multiValue({ key: option.key })}
                                    />
                                    <span className="slider round"></span>
                                </label>
                                <span style={{ display: 'block', position: 'relative', margin: '4px' }}>
                                    {option.name}
                                </span>
                            </li>

                        ))}</ul></p>
                        : null}
                    <p>
                        <span className="w3-xlarge">Стоимость:&nbsp;</span>
                        <span className="w3-xxlarge">{this.props.calculatePrice()} рублей</span>
                    </p>
                </div>

            </div>

            <div className="w3-row">
                <div className="w3-col l12 m12 s12">
                    <p>
                        <button className="w3-btn w3-ripple w3-red w3-large w3-right btn-green" type="button"
                            style={{ width: '50%' }} onClick={() => this.props.send()}>
                            Заказать
                </button>
                    </p>
                </div>
            </div>
            <br />
            <div className="w3-row">
                <div className="w3-col l12 m12 s12">
                    <div className="w3-panel w3-leftbar w3-light-grey">
                        <p className="w3-medium w3-serif">
                            <i>Нажимая на кнопку "Заказать" Вы подтверждаете что ознакомились и полностью согласны с
                        условиями <a href="#">публичной
                            оферты</a> и <a href="#">согласием на обработку персональных данных</a></i>
                        </p>
                    </div>
                </div>
            </div>

            <br />
        </div>
    }

}