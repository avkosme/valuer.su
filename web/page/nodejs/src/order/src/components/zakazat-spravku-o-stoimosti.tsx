import * as React from "react";
import { RequestsActions } from "../requests/";
import * as request from "superagent";
import Form from "./form";


export interface OrderProps {
    firstName: string;
    phoneNumber: string;
    email: string;
    description: string;
    files: object[];
    props: string[];
    pays: string[];
    price: object;
    priceAdd: object;
    pricePolicy: object[];
    nomenclatureOptions: object[];
    nomenclature: object[];
}

export interface Error {
    firstName: string;
    phoneNumber: string;
    email: string;
    pay: string;
    [key: string]: string;
}

export interface OrderState {
    firstName: string;
    phoneNumber: string;
    email: string;
    description: string;
    pay: string;
    error: Error;
    files: object[];
    price: number;
    options: number[];
    nomenclature: object[];
    view: string;
}



const R = new RequestsActions();

export class Order extends React.Component<OrderProps, OrderState> {
    constructor(props: any) {
        super(props);
        this.state = {
            firstName: props.firstName,
            phoneNumber: props.phoneNumber,
            email: props.email,
            description: props.description,
            pay: '',
            error: { firstName: '', phoneNumber: '', email: '', pay: '' },
            files: this.props.files,
            price: 0,
            options: this.props.nomenclatureOptions.map((option: any) => (option.status === "on" ? option.key : null)),
            nomenclature: this.props.nomenclature,
            view: 'form'
        }
    }

    updateState = (props: any) => (this.setState({ ...this.state, ...props }), this.validate(props))

    multiValue = (props: any) => {
        if (this.state.options.includes(props.key)) {
            this.updateState({ options: this.state.options.filter(e => e !== props.key) })
        } else {
            this.state.options.push(props.key)
            this.updateState({ options: this.state.options })
        }
    }

    calculatePrice = () => {
        if (this.state.options) {
            let sum: any = this.props.nomenclatureOptions.map((option: any) => (
                this.state.options.includes(option.key) ? this.state.price * option.ratio - this.state.price : null
            ))

            sum = sum.reduce((a: number, b: number) => a + b)
            return sum + this.state.price
        }
    }


    send = () => (this.validate(this.state) ? R.postRequest(this.state, '/api/?action=save').then(json => (this.updateState({ view: 'jimbo' }))) : false)

    validateIsEmpty = (value: string) => (!value || value === '' ? true : false)
    validateEmail = (email: string) => {
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    validatePhone = (phone: string) => {
        let re = /^((\+7|7|8)+([0-9]){10})$/;
        return re.test(String(phone));
    }

    validateRussian = (string: string) => {
        let re = /^(([А-я])+)$/;
        return re.test(String(string).toLowerCase());
    }

    validate = (props: any) => {
        let _err: any = { ...this.state.error }

        if ('firstName' in props) {
            delete _err['firstName']
            if (this.validateIsEmpty(props.firstName)) {
                _err = { ..._err, firstName: 'Необходимо указать имя' }
            }
            if (!this.validateRussian(props.firstName)) {
                _err = { ..._err, firstName: 'Введите имя на русском языке' }
            }
        }

        if ('phoneNumber' in props) {
            delete _err['phoneNumber']
            if (this.validateIsEmpty(props.phoneNumber)) {
                _err = { ..._err, phoneNumber: 'Необходимо указать номер телефона' }
            }

            if (!this.validatePhone(props.phoneNumber)) {
                _err = { ..._err, phoneNumber: 'Введите корректный номер телефона (должен начинаться с 8 или +7)' }
            }
        }

        if ('email' in props) {
            delete _err['email']
            if (this.validateIsEmpty(props.email)) {
                _err = { ..._err, email: 'Необходимо указать адрес электронной почты' }
            }

            if (!this.validateEmail(props.email)) {
                _err = { ..._err, email: 'Введен не корректный email' }
            }
        }

        if ('pay' in props) {
            delete _err['pay']
            if (this.validateIsEmpty(props.pay)) {
                _err = { ..._err, pay: 'Необходимо указать способ оплаты' }
            }
        }

        this.setState({ error: _err });

        return !Object.keys(_err).length
    }

    handlerMessage = (props: any) => {
        let alert = document.getElementById('bottom-alert-message')
        alert.innerHTML = props.message
        alert.style.display = 'block'
    }

    fileUpload = (event: any) => {
        event.preventDefault()
        let target = event.target
        let files = target.files
        // convert to array
        files = Array.prototype.slice.call(files, 0)

        this.upload(files)

    }

    upload = (files: any) => {
        let req = request.post('/api/?action=upload')
            .set({ 'X-CSRFToken': R.getCoockie().csrftoken })
        files.forEach((file: any) => {
            req.attach('file', file)
            req.on('progress', (e) => {
                let progress = (e.loaded / e.total) * 100
                /*
                if (!isNaN(progress)) {
                    this.handleProgress(progress)
                }
                */
            })
        })
        req.end((e, response) => {
            if (response.status === 401) {
                return window.location.href = `/logout/`
            }
            if (response.status === 200) {
                this.setState({ files: response.body.data })
                this.handlerMessage({ message: response.body.message })
            }
            return true
        })
    }

    getErrorClass = (name: string) => (this.state.error[name] ? 'w3-pale-red' : '')

    render() {
        switch (this.state.view) {
            case 'form': {
                return <Form
                    {...this.state}
                    pricePolicy={this.props.pricePolicy}
                    nomenclatureOptions={this.props.nomenclatureOptions}
                    getErrorClass={this.getErrorClass}
                    updateState={this.updateState}
                    calculatePrice={this.calculatePrice}
                    send={this.send}
                    multiValue={this.multiValue}
                    fileUpload={this.fileUpload}
                />
            }

            case 'jimbo': {
                return <div className="w3-section">
                    <div className="w3-wide bgimg w3-grayscale-min w3-center">
                        <h1 className="w3-jumbo">Ваша заявка успешно отправлена!</h1>
                    </div>
                </div>

            }
        }
    }
}
