
export class RequestsActions {

    getCoockie() {
        let str = document.cookie.split('; ')
        let result: any = {}
        for (let i = 0; i < str.length; i++) {
            let cur = str[i].split('=')
            result[cur[0]] = cur[1]
        }
        return result
    }

    postRequest(params: object, url: string) {
        return fetch(
            url,
            {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json;charset=UTF-8',
                    'X-CSRFToken': this.getCoockie().csrftoken
                },
                body: JSON.stringify(params)
            }
        )
            .then(response => {
                switch (response.status) {
                    case 401: {
                        return window.location.replace('logout/')
                    }
                    case 200: {
                        return response.json()
                    }
                }
            })
            .catch(e => {
                console.log(e)
            })

    }

}

function getCoockie() {
    let str = document.cookie.split('; ')
    let result: any = {}
    for (let i = 0; i < str.length; i++) {
        let cur = str[i].split('=')
        result[cur[0]] = cur[1]
    }
    return result
}

function postRequest(params: object, url: string) {
    return fetch(
        url,
        {
            method: 'post',
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                'X-CSRFToken': getCoockie().csrftoken
            },
            body: JSON.stringify(params)
        }
    )
        .then(response => {
            switch (response.status) {
                case 401: {
                    return window.location.replace('logout/')
                }
                case 200: {
                    return response.json()
                }
            }
        })
        .catch(e => {
            console.log(e)
        })
}