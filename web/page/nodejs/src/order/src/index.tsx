import * as React from "react";
import * as ReactDOM from "react-dom";

import { Order } from "./components/zakazat-spravku-o-stoimosti";

declare global {
    interface Window {
        firstName: string;
        phoneNumber: string;
        email: string;
        description: string;
        files: object[];
        props: string[];
        pays: string[];
        price: object;
        priceAdd: object;
        pricePolicy: object[];
        nomenclatureOptions: object[];
        nomenclature: object[];
    }
}

ReactDOM.render(
    <Order
        firstName={window.firstName}
        phoneNumber={window.phoneNumber}
        email={window.email}
        description={window.description}
        files={window.files}
        props={window.props}
        pays={window.pays}
        price={window.price}
        priceAdd={window.priceAdd}
        pricePolicy={window.pricePolicy}
        nomenclatureOptions={window.nomenclatureOptions}
        nomenclature={window.nomenclature}
    />,
    document.getElementById("order-root")
);
