from django.contrib import admin

from nomenclature.models import Nomenclature, Options


class NomenclatureAdmin(admin.ModelAdmin):
    list_display = ('sort', 'name', 'status')


admin.site.register(Nomenclature, NomenclatureAdmin)
admin.site.register(Options)
