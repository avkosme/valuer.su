from enum import Enum
from django.db import models
from page.models import Page


class Options(models.Model):
    name = models.CharField(
        verbose_name='Название',
        help_text='Название',
        max_length=500,
        blank=False
    )
    description = models.TextField(
        verbose_name='Описание',
        help_text='Описание',
        blank=False
    )

    class Statuses(Enum):
        off = ('off', 'Отключено')
        on = ('on', 'Включено')

        @classmethod
        def get_value(cls, member, number=0):
            return cls[member].value[number]

    status = models.CharField(
        verbose_name='Статус',
        help_text='Статус',
        choices=[x.value[0:2] for x in Statuses],
        max_length=10,
        default=Statuses.get_value('off')

    )

    status_default = models.CharField(
        verbose_name='Статус по-умолчанию',
        help_text='Статус по-умолчанию',
        choices=[x.value[0:2] for x in Statuses],
        max_length=10,
        default=Statuses.get_value('off')

    )

    ratio = models.DecimalField(
        verbose_name='Коэффициент',
        help_text='Коэффициент',
        max_digits=12,
        decimal_places=2,
        default=False,
        blank=False,
    )

    def __str__(self):
        return ' '.join((str(self.name), '-', str(self.Statuses.get_value(self.status, 1))))

    class Meta:
        verbose_name = 'Опции'
        verbose_name_plural = 'Опции'


class Nomenclature(models.Model):
    name = models.CharField(
        verbose_name='Название',
        help_text='Название',
        max_length=500,
        blank=False
    )
    description = models.TextField(
        verbose_name='Описание',
        help_text='Описание',
        blank=False
    )

    long_description = models.TextField(
        verbose_name='Описание',
        help_text='Описание',
        blank=False,
        default=False
    )

    class Statuses(Enum):
        off = ('off', 'Отключено')
        on = ('on', 'Включено')

        @classmethod
        def get_value(cls, member, number=0):
            return cls[member].value[number]

    status = models.CharField(
        verbose_name='Статус',
        help_text='Статус',
        choices=[x.value[0:2] for x in Statuses],
        max_length=10,
        default=Statuses.get_value('off')

    )
    price = models.DecimalField(
        verbose_name='Стоимость',
        help_text='Стоимость',
        max_digits=12,
        decimal_places=2,
        default=False,
        blank=False,
    )
    sort = models.IntegerField(
        verbose_name='Сортировка',
        help_text='Сортировка',
        default=1000,
        blank=False,
    )

    page = models.OneToOneField(
        Page,
        on_delete=models.CASCADE,
        primary_key=True,
        default=False,
        blank=True
    )

    options = models.ManyToManyField(
        Options,
        verbose_name='Опции',
        help_text='Опции',
    )

    def __str__(self):
        return ' '.join((str(self.sort), str(self.name), '-', self.Statuses.get_value(str(self.status), 1)))

    class Meta:
        verbose_name = 'Номенклатура'
        verbose_name_plural = 'Номенклатура'
