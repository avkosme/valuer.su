from enum import Enum
from django.db import models
from django.contrib.postgres.fields import JSONField
from django.contrib.postgres.fields import ArrayField
from users.models import User
from pay.models import Pay


class Order(models.Model):
    class Statuses(Enum):
        create = ('create', 'Создана')
        send = ('send', 'Отправлена')
        completed = ('completed', 'Завершена')

        @classmethod
        def get_value(cls, member, number=0):
            return cls[member].value[number]

    status = models.CharField(
        verbose_name='Статус',
        help_text='Статус',
        choices=[x.value[0:2] for x in Statuses],
        max_length=10,
        default=Statuses.get_value('create')
    )
    description = models.TextField(
        help_text='Дополнительная информация',
        verbose_name='Дополнительная информация',
        max_length=10000,
        blank=True
    )

    file = ArrayField(
        models.CharField(max_length=255),
        blank=True,
        default=list
    )
    nomenclature = JSONField()
    options = JSONField(blank=True, default=dict)
    user_data = JSONField()
    pay = models.ForeignKey(
        Pay,
        on_delete=models.CASCADE
    )

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )
