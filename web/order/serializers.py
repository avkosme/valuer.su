import json
from rest_framework import serializers
from django.forms.models import model_to_dict
from .models import Order
from pay.models import Pay


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'

    @staticmethod
    def clean_data(**kwargs):
        data = kwargs.get('data')
        user = kwargs.get('user')

        nomenclature = {'nomenclature': {'pk': data.get('nomenclature').get('key')}}
        pay = Pay.objects.get(alias=data.get('pay'))

        user.date_joined = user.date_joined.strftime("%Y-%m-%d %H:%M:%S")
        user.last_login = user.last_login.strftime("%Y-%m-%d %H:%M:%S") if user.last_login else ''
        user_data = {'user_data': json.dumps({**model_to_dict(user)})}

        return {**nomenclature, **user_data, 'pay': pay.pk, 'user': user.pk}
