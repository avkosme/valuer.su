from uuid import uuid4
from rest_framework import serializers
from file.models import File, Image


class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = ('file',)

    def save(self, **kwargs):
        _files = []
        for file in kwargs.get('data').getlist('file'):
            _name = uuid4()
            _files.append({'origin': file.name, 'name': _name})
            _file = File.objects.create(
                dir='/'.join(('user', str(kwargs.get('user_by_token')[1].pk))),
                name= file.name,
                user=kwargs.get('user_by_token')[1]
            )
            _file.file.save(name=file.name, content=file)
        return _files
