import datetime
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser, FormParser, JSONParser
from users.models import User
from .serializers import FileSerializer
from users.serializers import UserSerializer, PhoneSerializer
from order.serializers import OrderSerializer


class Api(APIView):
    STATUS_OK = 'ok'
    STATUS_ERR = 'err'
    parser_classes = (MultiPartParser, FormParser, JSONParser)

    def __init__(self):
        super().__init__()
        self.message = ''
        self.data = []
        self.action = ''

    def post(self, request, *args, **kwargs):

        try:
            query_string = request.META.get('QUERY_STRING')
            query_string = query_string.split('=')
            self.action = dict([query_string]).get('action')
        except ValueError:
            pass
        except KeyError:
            pass

        user_by_token = User.user_by_token(token=request.COOKIES.get('Token'))

        # File upload
        if self.action == 'upload':
            file_serializer = FileSerializer(data=request.data)
            try:
                assert file_serializer.is_valid()
                self.message = 'Файлы успешно загружены'
                self.data = file_serializer.save(data=request.data, user_by_token=user_by_token)
            except AssertionError:
                self.message = 'Произошла ошибка при загрузке файлов, попробуйте снова'

        # Save data
        if self.action == 'save':
            data = UserSerializer.clean_data(request.data)
            user_serializer = UserSerializer(data=data, instance=user_by_token[1])

            try:
                assert user_serializer.is_valid()
                if user_serializer.save():
                    phone_serializer = PhoneSerializer(data=data)
                    if phone_serializer.is_valid():
                        if phone_serializer.save(data=data, user=user_by_token[1]):
                            pass
            except AssertionError:
                self.message = 'Введенные данные ошибочны, проверьте данные, попробуйте снова'

            order_data = OrderSerializer.clean_data(data=data, user=user_by_token[1])
            order_serializer = OrderSerializer(data=order_data)

            if order_serializer.is_valid():
                if order_serializer.save():
                    self.message = 'Успешно сохранено'

        response = Response(
            {'status': Api.STATUS_OK, 'message': self.message, 'data': self.data},
            status=status.HTTP_200_OK,
            content_type='application/json; charset=utf-8',
            headers={
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'Content-Type',
            }
        )

        response.set_cookie('Token', user_by_token[0].key, max_age=86400,
                            expires=datetime.datetime.now() + datetime.timedelta(days=1))
        return response
