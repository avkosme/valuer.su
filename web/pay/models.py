from enum import Enum
from django.db import models
from nomenclature.models import Nomenclature


class Pay(models.Model):
    name = models.CharField(
        help_text='Название',
        verbose_name='Название',
        max_length=500,
    )

    alias = models.CharField(
        help_text='Алиас названия',
        verbose_name='Алиас названия',
        max_length=500,
        unique=True
    )

    class Statuses(Enum):
        off = ('off', 'Отключено')
        on = ('on', 'Включено')

        @classmethod
        def get_value(cls, member, number=0):
            return cls[member].value[number]

    status = models.CharField(
        verbose_name='Статус',
        help_text='Статус',
        choices=[x.value[0:2] for x in Statuses],
        max_length=10,
        default=Statuses.get_value('off')

    )

    def __str__(self):
        return ' '.join((str(self.name)))

    class Meta:
        verbose_name = 'Платежная система'
        verbose_name_plural = 'Платежная система'


class PricePolicy(models.Model):
    nomenclature = models.ForeignKey(
        Nomenclature,
        on_delete=models.CASCADE,
        help_text='Номенклатура',
        verbose_name='Номенклатура'
    )

    pay = models.ForeignKey(
        Pay,
        on_delete=models.CASCADE,
        help_text='Платежная система',
        verbose_name='Платежная система'
    )
    price = models.DecimalField(
        verbose_name='Стоимость',
        help_text='Стоимость',
        max_digits=12,
        decimal_places=2,
        default=False,
        blank=False,
    )

    def __str__(self):
        return ' '.join((str(self.nomenclature), str(self.pay), str(self.price)))

    class Meta:
        verbose_name = 'Ценовая политика'
        verbose_name_plural = 'Ценовая политика'
