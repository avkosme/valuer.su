from django.contrib import admin
from pay.models import Pay, PricePolicy

admin.site.register(Pay)
admin.site.register(PricePolicy)
