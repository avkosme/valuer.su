import os
from django.db import models
from django.core.validators import FileExtensionValidator
from users.models import User


def upload_to_file(instance, filename):
    return os.path.join(str(instance.dir), filename)


class File(models.Model):
    name = models.CharField(max_length=255)
    dir = models.CharField(max_length=255, default='', blank=False, null=False)
    file = models.FileField(blank=False, null=False, upload_to=upload_to_file,
                            validators=[FileExtensionValidator(
                                allowed_extensions=['pdf', 'doc', 'docx', 'jpg', 'png', 'xlsx', 'xls', 'odt', 'txt',

                                                    'zip'])])
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )

    @staticmethod
    def get_file_by_token(**kwargs):
        files = File.objects.filter(user__auth_token__key=kwargs.get('token')).all()
        return [{'origin': file.name, 'name': file.name} for file in files]


class Image(models.Model):
    name = models.CharField(max_length=255)
    image = models.ImageField(blank=False, null=False, upload_to='image')
